import { Component, OnInit } from '@angular/core';
import { Message } from '../models/message';
import { WebserviceService } from '../webservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.page.html',
  styleUrls: ['./new-message.page.scss'],
})
export class NewMessagePage implements OnInit {

  message = new Message();

  constructor(
    public webservice: WebserviceService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  async submit() {
    await this.webservice.postMessage(this.message)
      .then((message: Message) => {
        this.router.navigateByUrl('/all-messages');
      });
  }
  

}
