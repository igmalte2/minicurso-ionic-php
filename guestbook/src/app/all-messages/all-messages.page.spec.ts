import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMessagesPage } from './all-messages.page';

describe('AllMessagesPage', () => {
  let component: AllMessagesPage;
  let fixture: ComponentFixture<AllMessagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMessagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMessagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
