import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMessagePage } from './edit-message.page';

describe('EditMessagePage', () => {
  let component: EditMessagePage;
  let fixture: ComponentFixture<EditMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMessagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
